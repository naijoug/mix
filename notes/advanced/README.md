
## notes

- [ai](ai/README.md)
- [blockchain](blockchain/README.md)
- [OpenCV](topics/OpenCV.md)
- [OpenGL](topics/OpenGL.md)
- [ROS](topics/ROS.md)
- [Hardware](topics/Hardware.md)