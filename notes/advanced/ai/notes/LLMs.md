# LLMs 
> Large Language Models (大语言模型)

## Reference

- [Open LLMs](https://github.com/eugeneyan/open-llms)
    > 🤖 A list of open LLMs available for commercial use.   
- [Z-Bench](https://github.com/zhenbench/z-bench)
    > Z-Bench 1.0 by 真格基金：一个麻瓜的大语言模型中文测试集。Z-Bench is a LLM prompt dataset for non-technical users, developed by an enthusiastic AI-focused team in Zhenfund

## GPT

- [GPT4All](https://github.com/nomic-ai/gpt4all) : [Official](https://gpt4all.io)
    > gpt4all: an ecosystem of open-source chatbots trained on a massive collections of clean assistant data including code, stories and dialogue
- [minichatgpt](https://github.com/juncongmoo/minichatgpt) 
    > 🔥 To Train ChatGPT In 5 Minutes with ColossalAI
- [privateGPT](https://github.com/imartinez/privateGPT)
    > Interact privately with your documents using the power of GPT, 100% privately, no data leaks

## LLaMa

- [LLaMA](https://github.com/facebookresearch/llama) : [introducing](https://ai.facebook.com/blog/large-language-model-llama-meta-ai/)
    > Inference code for LLaMA models
- [llama.cpp](https://github.com/ggerganov/llama.cpp)
    > Port of Facebook's LLaMA model in C/C++
- [ChatLLaMa](https://github.com/juncongmoo/chatllama)
    > ChatLLaMA 📢 Open source implementation for LLaMA-based ChatGPT runnable in a single GPU. 15x faster training process than ChatGPT
- [pyllama](https://github.com/juncongmoo/pyllama)
    > 🦙 LLaMA - Run LLM in A Single 4GB GPU

## Alpaca

- [Stanford Alpaca](https//github.com/tatsu-lab/stanford_alpaca)
    > An Instruction-following LLaMA Model
- [alpaca-lora](https://github.com/tloen/alpaca-lora)
    > Instruct-tune LLaMA on consumer hardware
- [Chinese-LLaMA-Alpaca](https://github.com/ymcui/Chinese-LLaMA-Alpaca)
    > 中文LLaMA&Alpaca大语言模型+本地CPU/GPU部署 (Chinese LLaMA & Alpaca LLMs)
- [KoAlpaca](https://github.com/Beomi/KoAlpaca)
    > KoAlpaca: Korean Alpaca Model based on Stanford Alpaca (feat. LLAMA and Polyglot-ko)

## Vicuna

- [FastChat](https://github.com/lm-sys/FastChat)
    > An open platform for training, serving, and evaluating large language models. Release repo for Vicuna and FastChat-T5.
- [Chinese-Vicuna](https://github.com/Facico/Chinese-Vicuna)
    > Chinese-Vicuna: A Chinese Instruction-following LLaMA-based Model —— 一个中文低资源的llama+lora方案，结构参考alpaca
- [WizardVicunaLM](https://github.com/melodysdreamj/WizardVicunaLM)
    > LLM that combines the principles of wizardLM and vicunaLM

## Other

- [ChatGLM-6B](https://github.com/THUDM/ChatGLM-6B)
    > ChatGLM-6B: An Open Bilingual Dialogue Language Model | 开源双语对话语言模型
- [Luotuo-Chinese-LLM](https://github.com/LC1332/Luotuo-Chinese-LLM)
    > 骆驼(Luotuo): Open Sourced Chinese Language Models
- [Visual OpenLLM](https://github.com/visual-openllm/visual-openllm)
    > something like visual-chatgpt, 文心一言的开源版
- [wenda](https://github.com/wenda-LLM/wenda)
    > 闻达：一个LLM调用平台。为小模型外挂知识库查找和设计自动执行动作，实现不亚于于大模型的生成能力
- [Linly](https://github.com/CVI-SZU/Linly)
    > Chinese-LLaMA基础模型；ChatFlow中文对话模型；中文OpenLLaMA模型；NLP预训练/指令微调数据集
- [CPM-Bee](https://github.com/OpenBMB/CPM-Bee)
    > 百亿参数的中英文双语基座大模型
    
## Tutorial

- [Full Stack LLM Bootcamp](https://github.com/the-full-stack/website)
- [基于本地知识库的 ChatGLM 等大语言模型应用实现](https://github.com/imClumsyPanda/langchain-ChatGLM)
- [一种平价的chatgpt实现方案, 基于ChatGLM-6B + LoRA](https://github.com/mymusise/ChatGLM-Tuning)
- [2023-05-18 GPT大语言模型Vicuna本地化部署实践（效果秒杀Alpaca）](https://zhuanlan.zhihu.com/p/630287397)
- [2023-04-22 大模型也内卷，Vicuna训练及推理指南，效果碾压斯坦福羊驼](https://zhuanlan.zhihu.com/p/624012908)
- [2023-01-18 通向AGI之路：大型语言模型（LLM）技术精要](https://zhuanlan.zhihu.com/p/597586623)
- [2019-01-27 BERT大火却不懂Transformer？读这一篇就够了](https://zhuanlan.zhihu.com/p/54356280)
- [2019-01-13 放弃幻想，全面拥抱Transformer：自然语言处理三大特征抽取器（CNN/RNN/TF）比较](https://zhuanlan.zhihu.com/p/54743941)