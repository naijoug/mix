# Prompt

## Reference

- [🧠 Awesome ChatGPT Prompts](https://github.com/f/awesome-chatgpt-prompts)
    > This repo includes ChatGPT prompt curation to use ChatGPT better.
- [Prompt Engineering Guide](https://github.com/dair-ai/Prompt-Engineering-Guide)
    > 🐙 Guides, papers, lecture, notebooks and resources for prompt engineering
- [⚡️AI Short](https://github.com/rockbenben/ChatGPT-Shortcut)
    > 🚀💪Maximize your efficiency and productivity, support for English, 中文, 日本語, and 한국어. 让生产力加倍的 ChatGPT 快捷指令，按照领域和功能分区，可对提示词进行标签筛选、关键词搜索和一键复制。
- [ClickPrompt](https://github.com/prompt-engineering/click-prompt)
    > ClickPrompt - Streamline your prompt design, with ClickPrompt, you can easily view, share, and run these prompts with just one click. ClickPrompt 用于一键轻松查看、分享和执行您的 Prompt。
- [Prompt 编写模式](https://github.com/prompt-engineering/prompt-patterns)
    > Prompt 编写模式：如何将思维框架赋予机器，以设计模式的形式来思考 prompt
- [理解 Prompt](https://github.com/prompt-engineering/understand-prompt)
    > 理解 Prompt：基于编程、绘画、写作的 AI 探索与总结

## [GPT Engineer](https://github.com/AntonOsika/gpt-engineer)
> Specify what you want it to build, the AI asks for clarification, and then builds it.

```shell
# install 
$ pip install gpt-engineer

# 新建项目文件夹
$ mkdir project_folder
# 创建项目 prompt 项目指令
$ vim project_folder/prompt
# 运行 gpt-engineer
$ gpt-engineer project_folder/
```