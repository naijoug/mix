# AIGC

## Reference

- [lora](https://github.com/cloneofsimo/lora)
    > Using Low-rank adaptation to quickly fine-tune diffusion models.
- [InvokeAI](https://github.com/invoke-ai/InvokeAI)
    > InvokeAI is a leading creative engine for Stable Diffusion models, empowering professionals, artists, and enthusiasts to generate and create visual media using the latest AI-driven technologies. The solution offers an industry leading WebUI, supports terminal use through a CLI, and serves as the foundation for multiple commercial products.
- [🌈通往 AGC 之路](https://waytoagi.feishu.cn/wiki/QPe5w5g7UisbEkkow8XcDmOpn8e)