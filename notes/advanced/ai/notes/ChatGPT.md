# ChatGPT

## Reference

- [chatGPT](chat.openai.com)
- [ChatGPT-API-Faucet](https://github.com/terobox/ChatGPT-API-Faucet)
    > AI 圈的水龙头网站，每24小时可领取一个令牌用于开发测试 AI 产品
- [OpenAI Cookbook](https://github.com/openai/openai-cookbook)
    > Examples and guides for using the OpenAI API
- [Chat GPT Demo](https://chatgptdemo.net)
- [Awesome-ChatGPT](https://github.com/runningcheese/Awesome-ChatGPT)
- [ChatGPT-Mac-MenuBar](https://github.com/KittenYang/ChatGPT-Mac-MenuBar)
- [wechat-chatgpt](https://github.com/fuergaosi233/wechat-chatgpt)
- [chat-gpt-google-extension](https://github.com/wong2/chat-gpt-google-extension)

## Fake

- [文心一言](https://yiyan.baidu.com/)
- [通义千问](https://tongyi.aliyun.com/)
- [COGNOSYS](https://www.cognosys.ai)

## whisper

- [wisper](https://github.com/openai/whisper)
    > Robust Speech Recognition via Large-Scale Weak Supervision
- [whisper.cpp](https://github.com/ggerganov/whisper.cpp)
    > Port of OpenAI's Whisper model in C/C++
- [whisper-timestamped](https://github.com/linto-ai/whisper-timestamped)
    > Multilingual Automatic Speech Recognition with word-level timestamps and confidence

## Tools

- [ChatALL](https://github.com/sunner/ChatALL)
    > Concurrently chat with ChatGPT, Bing Chat, bard, Alpaca, Vincuna, Claude, ChatGLM, MOSS, iFlytek Spark, ERNIE and more, discover the best answers
- [Open-Assistant](https://github.com/LAION-AI/Open-Assistant)
    > OpenAssistant is a chat-based assistant that understands tasks, can interact with third-party systems, and retrieve information dynamically to do so.
- [Chat2DB](https://github.com/alibaba/Chat2DB)
    > 智能的通用数据库工具和SQL客户端（General-purpose database tools and SQL clients with AI (ChatGPT)）
- [DB-GPT](https://github.com/csunny/DB-GPT)
    > Interact your data and environment using the local GPT, no data leaks, 100% privately, 100% security
- [polyglot](https://github.com/liou666/polyglot)
    > 🤖️ 桌面端AI语言练习应用
- [myGPTReader](https://github.com/madawei2699/myGPTReader)
    > A community-driven way to read and chat with AI bots - powered by chatGPT.

## Tutorial

- [使用腾讯云函数一分钟搭建 OpenAI 免翻墙代理](https://github.com/Ice-Hazymoon/openai-scf-proxy)
- [2023-05-24 拥有自我意识的AI：AutoGPT](https://juejin.cn/post/7236594708301840441)
- [2023-05-21 我与ChatGPT结对编程的体验](https://www.bmpi.dev/dev/chatgpt-development-notes/pair-programming)
- [2023-05-17 70款ChatGPT插件评测](https://zhuanlan.zhihu.com/p/629337429)
- [2023-05-14 简单实现一个ChatGPT Plugin](https://zhuanlan.zhihu.com/p/629207240)
- [2023-04-07 ChatGPT应用开发小记](https://www.bmpi.dev/dev/chatgpt-development-notes/my-gpt-reader/)
- [2023-06-10 ChatGPT实践应用和大模型技术解析](https://live.juejin.cn/4354/ChatGPT)
