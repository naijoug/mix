# NLP
> Natural Language Processing : 自然语言处理

> - **Wikipedia**: 自然语言处理（英语：Natural Language Processing，缩写作 NLP）是人工智慧和语言学领域的分支学科。此领域探讨如何处理及运用自然语言；自然语言处理包括多方面和步骤，基本有认知、理解、生成等部分。
> - **Baidu**: 自然语言处理( Natural Language Processing, NLP)是以语言为对象，利用计算机技术来分析、理解和处理自然语言的一门学科,即把计算机作为语言研究的强大工具，在计算机的支持下对语言信息进行定量化的研究,并提供可供人与计算机之间能共同使用的语言描写。包括自然语言理解( NaturalLanguage Understanding, NLU)和自然语言生成( Natural LanguageGeneration, NLG)两部分。
> - **Google**: 作为人工智能技术的分支，NLP（自然语言处理）使用机器学习来处理和解释文本和数据。自然语言识别和自然语言生成是 NLP 的类型。
> - **Amazon**: 自然语言处理(NLP) 是一种机器学习技术，使计算机能够解读、处理和理解人类语言。
> - **Huawei**: 自然语言处理（Natural Language Processing，简称NLP）是一款基于人工智能技术，针对各类企业及开发者提供的用于文本分析及挖掘的云服务，旨在帮助用户高效的处理文本。
> - **Orace**: 自然语言处理 (NLP) 是人工智能 (AI) 的一个分支，它能够使计算机理解、生成和处理人类语言，支持用户使用自然语言文本或语音来询问 (interrogate) 数据，因此又被称为“语言输入 (language in)”。
> - **IBM**: 自然语言处理(NLP) 是计算机科学的一个分支，更具体地说，是人工智能(AI)的分支，目标是让计算机能够以与人类大致相同的方式理解文本和口语。
> - **Nvidia**: 自然语言处理(NLP) 使用 AI 处理和分析文本或语音数据，以便理解和解释内容、对内容进行分类和/或从内容中获得见解。

## Reference

- [spaCy](https://github.com/explosion/spaCy)
    > 💫 Industrial-strength Natural Language Processing (NLP) in Python
- [NLTK](https://github.com/nltk/nltk)
    > Natural Language Toolkit
- [Stanford CoreNLP](https://github.com/stanfordnlp/CoreNLP) 
    > A Java suite of core NLP tools.
- [Apache OpenNLP](https://github.com/apache/opennlp)
    > A machine learning based toolkit for the processing of natural language text.
- [Tokenizers](https://github.com/huggingface/tokenizers)
    > 💥 Fast State-of-the-Art Tokenizers optimized for Research and Production
- [fastText](https://github.com/facebookresearch/fastText)
    > Library for fast text representation and classification.
- [polyglot](https://github.com/aboSamoor/polyglot)
    > Multilingual text (NLP) processing toolkit
- [jieba](https://github.com/fxsjy/jieba) : Chinese
    > “结巴”中文分词：做最好的 Python 中文分词组件
- [LAC](https://github.com/baidu/lac) : Chinese
    > (Lexical Analysis of Chinese) 百度NLP：分词，词性标注，命名实体识别，词重要性
- [KoNLPy](https://github.com/konlpy/konlpy) : Korean  
    > Python package for Korean natural language processing.
- [Janome](https://github.com/mocobeta/janome) : Japanese
    > Japanese morphological analysis engine written in pure Python
- [CAMeL Tools](https://github.com/CAMeL-Lab/camel_tools) : Arabic
    > A suite of Arabic natural language processing tools developed by the CAMeL Lab at New York University Abu Dhabi.
- [PyThaiNLP](https://github.com/PyThaiNLP/pythainlp) : Thai
    > Thai Natural Language Processing in Python.
- [underthesea](https://github.com/undertheseanlp/underthesea)
    > Underthesea - Vietnamese NLP Toolkit
- [pyvi](https://github.com/trungtv/pyvi)
    > Python Vietnamese Core NLP Toolkit
- [VNLP](https://github.com/vngrs-ai/vnlp)  
    > State-of-the-art, lightweight NLP tools for Turkish language. Developed by VNGRS.
- [Malaya](https://github.com/huseinzol05/malaya)   
    > Natural Language Toolkit for bahasa Malaysia
- [indonesian-roberta-base-posp-tagger](https://huggingface.co/w11wo/indonesian-roberta-base-posp-tagger)   
    > Indonesian RoBERTa Base POSP Tagger

## embedding

- [LASER](https://github.com/facebookresearch/LASER)
    > Language-Agnostic SEntence Representations
- [gensim](https://github.com/RaRe-Technologies/gensim)
    > Topic Modelling for Humans

## Tutorial

- [HuggingFace NLP Course](https://huggingface.co/learn/nlp-course/zh-CN)
- [令人讚嘆的自然語言處理](https://github.com/keon/awesome-nlp/blob/master/README-ZH-TW.md)