# AI

## Reference

- [AI 工具集](https://ai-bot.cn/)

## notes

- [NLP](notes/NLP.md)
- [ML](notes/ML.md)
- [LLMs](notes/LLMs.md)
- [AIGC](notes/AIGC.md)
- [ChatGPT](notes/ChatGPT.md)
- [Prompt](notes/Prompt.md)

## Concept

| abbr | full | description
| --- | --- | ---
| `NLP`     | Natural Language Processing           | 自然语言处理
| `LLMs`    | Large Language Models                 | 大语言模型
| `GLM`     | General Language Model                | 通用语言模型
| `LoRA`    | Low-Rank Adaptation of Large Language Models | 大语言模型的低阶适应
| `BERT`    | Bidirectional Encoder Representations from Transformers | 基于大规模无监督预训练的双向表示法
| `GPT`     | Generative Pre-trained Transformer    | 生成式预训练语言模型
| `LLaMA`   | Large Language Model Meta AI          | facebook 大语言模型
| `RNN`     | Recurrent Neural Network              | 循环神经网络
| `CNN`     | Convolutional Neural Network          | 卷积神经网络
| `ELMO`    | Embeddings from Language Models       |  
| `SA`      | Self-Attention                        | 自注意力机制
| `F-FNN`   | Feed-Forward Neural Networks          | 前向神经网络
| `RC`      | Residual Connection                   | 残差连接
| `MLM`     | Masked Language Model                 | 掩码语言模型
| `NSP`     | Next Sentence Prediction              | 下一句预测
| `POS`     | Part of Speech                        | 词性
| `TF`      | Term Frequency                        | 词频
| `SW`      | Stop Words                            | 停用词
| `IDF`     | Inverse Document Frequency            | 逆文档频率
|           | Fine Tuning                           | 微调

## community

- [HuggingFace - 抱抱脸](https://huggingface.co/)
- [ModelScope - 魔搭平台](https://www.modelscope.cn)

## `TensorFlow` vs `PyTorch` vs  `Jax`

- [TensorFlow](https://github.com/tensorflow/tensorflow)
    > An Open Source Machine Learning Framework for Everyone
- [PyTorch](https://github.com/pytorch/pytorch)
    > Tensors and Dynamic neural networks in Python with strong GPU acceleration
- [Jax](https://github.com/google/jax)
    > Composable transformations of Python+NumPy programs: differentiate, vectorize, JIT to GPU/TPU, and more

## `Transformer` vs `RNN` vs `CNN`

- `Transformer` : (Multi-Head Attention) 多头自注意力机制
    > 特色 : 自然语言处理
- `RNN` : 循环神经网络
    > 特色 : 处理序列数据和时序关系
- `CNN` : 卷积神经网络
    > 特色 : 处理网格数据(如: 图像处理)

## `Streamlit` vs `Gradio`

- [Streamlit](https://github.com/streamlit/streamlit)
    > Streamlit — A faster way to build and share data apps.
- [Gradio](https://github.com/gradio-app/gradio)
    > Create UIs for your machine learning model in Python in 3 minutes
- [Stable Diffusion web UI](https://github.com/AUTOMATIC1111/stable-diffusion-webui)
    > A browser interface based on Gradio library for Stable Diffusion.
- [Reflex](https://github.com/reflex-dev/reflex)
    > (Previously Pynecone) 🕸 Web apps in pure Python 🐍

## Tools

- [Transformers](https://github.com/huggingface/transformers)
    > 🤗 Transformers: State-of-the-art Machine Learning for Pytorch, TensorFlow, and JAX. 为 Jax、PyTorch 和 TensorFlow 打造的先进的自然语言处理
- [ModelScope](https://github.com/modelscope/modelscope)
    > ModelScope: bring the notion of Model-as-a-Service to life.
- [LoRA](https://github.com/microsoft/LoRA)
    > Code for loralib, an implementation of "LoRA: Low-Rank Adaptation of Large Language Models"
- [LangChain](https://github.com/hwchase17/langchain)
    > ⚡ Building applications with LLMs through composability ⚡
- [embedchain](https://github.com/embedchain/embedchain)
    > Framework to easily create LLM powered bots over any dataset.
- [Lightning](https://github.com/lightning-ai/lightning)
    > Deep learning framework to train, deploy, and ship AI products Lightning fast.
- [Semantic Kernel](https://github.com/microsoft/semantic-kernel)
    > Integrate cutting-edge LLM technology quickly and easily into your apps
- [guidance](https://github.com/microsoft/guidance)
    > A guidance language for controlling large language models. 

## AI Coder

- [Github Copilot](https://github.com/features/copilot)
    > Your AI pair programmer
- [Amazon CodeWhisperer](https://aws.amazon.com/cn/codewhisperer)
- [Alibaba cosy](https://github.com/alibaba-cloud-toolkit/cosy) 
    > 阿里云智能编码插件（Alibaba Cloud AI Coding Assistant）是一款AI编程助手，它提供代码智能补全和IDE内的代码示例搜索能力，帮助你更快更高效地写出高质量代码。
- [CodeGeeX](https://github.com/THUDM/CodeGeeX)
    > CodeGeeX: An Open Multilingual Code Generation Model
- [Bito AI](https://github.com/gitbito/bitoai)
    > 🚀 Bito AI – Bring ChatGPT to your IDE to 10x your dev abilities!
- [CopilotForXcode](https://github.com/intitni/CopilotForXcode)
    > The missing GitHub Copilot, Codeium and ChatGPT Xcode Source Editor Extension
- [Continue](https://github.com/continuedev/continue)
    > ⏩ the open-source coding copilot—bring the power of ChatGPT to VS Code
- [Sweep](https://github.com/sweepai/sweep)
    > Sweep: AI-powered Junior Developer for small features and bug fixes.

## Tutorial

- [Microsoft - ML-For-Beginners](https://github.com/microsoft/ML-For-Beginners)
    > 12 weeks, 26 lessons, 52 quizzes, classic Machine Learning for all
- [Ali - 人工智能学习路线](https://developer.aliyun.com/learning/roadmap/ai)
- [course.fast.ai](https://github.com/fastai/course22)
- [2023-06-06 从零开始学LangChain](https://www.yuque.com/layingqingyangair/ssaoyc/eghmafr97u3po8xq)
- [2023-05-25 通过抬杠，了解文本摘要的实现原理](https://juejin.cn/post/7236694100216692795)
- [2023-05-20 快来尝试你的第一个AI程序](https://juejin.cn/post/7234852669021831227)