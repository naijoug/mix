## Reference

- [awesome-blockchain-cn](https://github.com/chaozh/awesome-blockchain-cn)
- [深入浅出区块链](https://learnblockchain.cn)
- [区块链技术博客](http://me.tryblockchain.org/)
- [巴比特图书](http://book.8btc.com/)

- [区块链技术指南](https://legacy.gitbook.com/book/yeasy/blockchain_guide/details)
- [区块链学习资源](https://bihu.com/article/67257)
- [Node.js 区块链开发](http://bitcoin-on-nodejs.ebookchain.org/)
- [区块链开发系列](https://github.com/ChengQuEducation/BlockChain)
- [区块链技术简史](https://ethfans.org/toya/articles/224)
- [圆方圆学院](https://my.csdn.net/yuanfangyuan_block)
- [区块链挖矿算法清单](http://demontf.github.io/2018/02/28/miningAlgorithmList)

## Website

- [Blockchain][web-01] : 比特币区块浏览器
- [CoinMarketCap][web-02] : 区块链货币市场排行
- [DAppWorld][web-03] : 最全最新的区块链应用平台
- [TokenWallet][web-04] : 数字货币钱包排行榜

[web-01]: https://blockchain.com
[web-02]: https://coinmarketcap.com
[web-03]: http://www.dappworld.com
[web-04]: http://tokenwallet.cc/

## Glossary

| 词汇 | 说明 
| --- | ---
| Satoshi Nakamoto | 中本聪，比特币创始人
| Satoshi | 聪，比特币货币单位(0.00000001 BTC 比特币最小金额，亿分之一比特币)
| 糖果🍬 | 赠送代币给注册的新用户
| 空投 | 直接按用户持有某币(BTC、ETH、EOS...)的数量，按比例赠送代币给的用户
| 薅羊毛 | 注册多个账户去领取各种代币的糖果🍬

| 词汇 | 说明
| --- | ---
| POW (Proof of Work) | 工作量证明 
| POS (Proof of Stake) | 权益证明 [stake: 权益]
| DPOS (Delegated Proof of Stake) | 委托权益证明
| POA (Proof-of-Authority) | 权威证明
| WIF (Wallet Import Format) | 导入钱包的格式 (私钥以 Base58 校验和编码格式)
| P2SH (Pay-to-Script-Hash) | 付款脚本
| DApp (Decentralized Applications) | 去中心化应用
| UTXO (Unspent Transaction Outputs) | 未经使用的交易输出
| BIP (Bitcoin Improvement Proposals) | 比特币改进提议 
| HD 钱包 (Hierarchical Deterministic Wallet) | 分层确定性钱包 [=>][ref-01]
| SPV (Simplified Payment Verification) | 简单支付验证(非交易验证)  [=>][ref-02]

[ref-01]: http://bjiebtc.com/bitebi-zhishidian-hdqianbao    (比特币安全知识点：HD钱包)
[ref-02]: http://www.8btc.com/spv_spv_node_and_spv_wallet   (SPV、SPV节点和SPV钱包)

## OpenSource

| iOS 开源项目 | 说明 
| --- | ---
| [breadwallet-ios][os-iOS-01] | Bread 开源的区块链 iOS 钱包
| [trust-wallet-ios][os-iOS-02] | 开源的 Ethereum(以太坊) iOS 钱包 
| [PocketEOS-IOS][os-iOS-03] | OracleChain(欧链) 开源的 EOS (iOS) 钱包
| [My-Wallet-V3-iOS][os-iOS-04] | Blockchain 开源的区块链钱包
| [bither-ios][os-iOS-05] | Bither 开源的 Bitcoin(比特币) iOS 钱包
| [arcbit-ios][os-iOS-06] | Arcbit 开源的 Bitcoin(比特币) iOS 钱包
| [KKWallet_iOS][os-IOS-07] | 以太坊 iOS 钱包

| Android 开源项目 | 说明 
| --- | ---
| [breadwallet-android][os-Android-01] | Bread 开源的区块链 Android 钱包
| [bitcoin-wallet][os-Android-02] | 开源比特币钱包(Android)，独立的比特币节点，不需要后端。
| [EosCommander][os-Android-03] | Plactal 开源的 EOS.IO 的 Android 客户端
| [PocketEOS-Android][os-Android-04] | OracleChain(欧链) 开源的 EOS (Android) 钱包
| [bither-android][os-Android-05] | Bither 开源的 Bitcoin(比特币) Android 钱包
| [arcbit-android][os-Android-06] | Arcbit 开源的 Bitcoin(比特币) Android 钱包

| 其它开源项目 | 说明 
| --- | ---
| [money-tree][os-Other-01] | HD Wallets by Ruby
| [EOSDevHelper][os-Other-02] | OracleChain(欧链) 开源的 EOS 桌面钱包开发工具
| [EosCannon-Offline-Tools][os-Other-03] | EOS 佳能离线工具
| [Scatter][os-Other-04] | 开源的区块链(EOS & Ethereum)浏览器插件交易工具
| [coinmon][os-Other-05] | 命令行查看加密货币最新价格 

[os-iOS-01]: https://github.com/breadwallet/breadwallet-ios
[os-iOS-02]: https://github.com/TrustWallet/trust-wallet-ios
[os-iOS-03]: https://github.com/OracleChain/PocketEOS-IOS
[os-iOS-04]: https://github.com/blockchain/My-Wallet-V3-iOS
[os-iOS-05]: https://github.com/bither/bither-ios
[os-iOS-06]: https://github.com/arcbit/arcbit-ios
[os-iOS-07]: https://github.com/KKwallet/KKWallet_iOS

[os-Android-01]: https://github.com/breadwallet/breadwallet-android
[os-Android-02]: https://github.com/bitcoin-wallet/bitcoin-wallet
[os-Android-03]: https://github.com/plactal/EosCommander
[os-Android-04]: https://github.com/OracleChain/PocketEOS-Android
[os-Android-05]: https://github.com/bither/bither-android
[os-Android-06]: https://github.com/arcbit/arcbit-android

[os-Other-01]: https://github.com/GemHQ/money-tree
[os-Other-02]: https://github.com/OracleChain/EOSDevHelper
[os-Other-03]: https://github.com/eoscannon/EosCannon-Offline-Tools
[os-Other-04]: https://github.com/EOSEssentials/Scatter
[os-Other-05]: https://github.com/bichenkk/coinmon.git

## BTC vs ETH vs EOS

| 区块链 | 名称 | github | whitepaper(白皮书) | 交易数/s | 区块生成(s) | gas 
| --- | --- | --- | --- | --- | --- | ---
| 1.0 | [比特币][btc-00] | [bitcoin][btc-01] | [en][btc-02] & [zh][btc-03] | 7     | 600 | -
| 2.0 | [以太坊][eth-00] | [ethereum][eth-01]| [en][eth-02] & [zh][eth-03] | 30    | -   | -
| 3.0 | [EOS][eos-00]   | [EOSIO][eos-01]   | [en][eos-02] & [zh][eos-03] | 百万   | 3   | 0

[btc-00]: https://bitcoin.org
[btc-01]: https://github.com/bitcoin
[btc-02]: https://bitcoincore.org/bitcoin.pdf
[btc-03]: http://www.8btc.com/wiki/bitcoin-a-peer-to-peer-electronic-cash-system

[eth-00]: https://www.ethereum.org/
[eth-01]: https://github.com/ethereum
[eth-02]: https://github.com/ethereum/wiki/wiki/White-Paper
[eth-03]: https://github.com/ethereum/wiki/wiki/%5B%E4%B8%AD%E6%96%87%5D-%E4%BB%A5%E5%A4%AA%E5%9D%8A%E7%99%BD%E7%9A%AE%E4%B9%A6

[eos-00]: https://eos.io
[eos-01]: https://github.com/EOSIO
[eos-02]: https://github.com/EOSIO/Documentation/blob/master/TechnicalWhitePaper.md 
[eos-03]: https://github.com/EOSIO/Documentation/blob/master/zh-CN/TechnicalWhitePaper.md

## IPFS
> (InterPlanetary File System) 星际文件系统

- [ipfs](https://github.com/ipfs/ipfs)
    > Peer-to-peer hypermedia protocol
- [ipfs-tutorial](https://github.com/miaoski/ipfs-tutorial)
- [分布式文件系统 IPFS 与 FileCoin](https://draveness.me/ipfs-filecoin)

## Steem
> The Blockchain That Will Tokenize The Web

- [steem](https://github.com/steemit/steem)
    > The blockchain for Smart Media Tokens (SMTs) and decentralized applications.
- [Steem Handbook](http://steemh.org)