# Media
> Video & Audio (音视频)

## Reference

- [免费在线音频文件转换器](https://products.aspose.app/audio/zh-cn/conversion)

## Live

- [2021.03.02 在线教室 iOS 端声音问题综合解决方案](https://juejin.cn/post/6934987607088726053#heading-12)
- [RTC-Developer](https://github.com/RTC-Developer)

- RTM (Real-Time Messaging) : 实时消息(信令)
    > 传输数据 : 消息文本或加密数据
    > 实时消息场景 : 消息发送、呼叫邀请
- RTC (Real-Time Communication) : 实时通信
    > 传输数据 : 音视频数据
    > 实时通信场景 : 语音通话、视频通话、音频互动直播、视频互动直播 
- RTMP/RTMPS (Real-Time Transmit Protocol Security) : 实时传输协议

## AVAssetResourceLoaderDelegate

- [2021.04.21 开发播放器框架之边下边播边存方案](https://github.com/yangKJ/KJPlayerDemo/wiki/%E5%BC%80%E5%8F%91%E6%92%AD%E6%94%BE%E5%99%A8%E6%A1%86%E6%9E%B6%E4%B9%8B%E8%BE%B9%E4%B8%8B%E8%BE%B9%E6%92%AD%E8%BE%B9%E5%AD%98%E6%96%B9%E6%A1%88)
- [2020.02.05 Understanding AVAssetResourceLoaderDelegate](https://github.com/2ZGroupSolutionsArticles/Article_EZ002)
- [2019.12.03 iOS AVPlayer 视频缓存的设计与实现](http://chuquan.me/2019/12/03/ios-avplayer-support-cache/)
- [2019.04.23 AVPlayer 边下边播与最佳实践](https://zltunes.github.io/2019/04/23/avplayer-best-practice/)
- [iOS实现在线视频边下边播](https://github.com/Kaibin/YYVideoPlayer)
- [2018.08.31 iOS短视频播放缓存之道](https://segmentfault.com/a/1190000016228456)
    > [ShortMediaCache](https://github.com/dangercheng/ShortMediaCache)
- [2018.02.28 AVPlayer初体验之边下边播与视频缓存](http://xferris.cn/avplayer_cache/)
- [2017.06.13 Swift 封装一个视频播放器 VGPlayer](https://www.jianshu.com/p/1680978e1a7e)
    > [VGPlayer](https://github.com/VeinGuo/VGPlayer)
- [2017.03.31 可能是目前最好的 `AVPlayer` 音视频缓存方案](https://mp.weixin.qq.com/s/v1sw_Sb8oKeZ8sWyjBUXGA?#%23)
    > [VIMediaCache](https://github.com/vitoziv/VIMediaCache)
- [2016.09.03 Implementing AVAssetResourceLoaderDelegate: a How-To Guide](https://jaredsinclair.com/2016/09/03/implementing-avassetresourceload.html)
- [2016.07.12 CachingPlayerItem](https://github.com/neekeetab/CachingPlayerItem)
- [2016.05.24 iOS音频播放 (九)：边播边缓存](https://msching.github.io/blog/2016/05/24/audio-in-ios-9/)

## Encoding

- [H264 vs H265](https://mattgadient.com/x264-vs-x265-vs-vp8-vs-vp9-examples/)
- [2019.07.05 iOS视频压缩笔记](https://www.jianshu.com/p/4f69c22c6dce)
- [File Size Issue with HEVC Encoder on iOS 11](https://medium.com/@jpetrichsr/file-size-issue-with-hevc-encoder-on-ios-11-f50104ace54f)