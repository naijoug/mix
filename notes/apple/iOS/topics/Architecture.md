# Architecture

## Reference

- [2018.05.29 Trip-to-iOS-Design-Patterns](https://github.com/skyming/Trip-to-iOS-Design-Patterns)
- [2018.02.04 iOS架构之View层的架构方案](https://mp.weixin.qq.com/s/t_IBkCClPBZFBPmtZT0WsQ)

## MVVM

- [2017.09.04 - [译] MVVM, Coordinators 和 RxSwift 的抽丝剥茧](https://juejin.cn/post/6844903494013435918)
- [2017.04.13 - [译] MVVM-C 与 Swift](https://juejin.cn/post/6844903473897537549)
- [2016.08.07 - iOS 开发中的 ViewModel](https://www.jianshu.com/p/823297d8c386)
- [2013.05.00 - MVVM - Commands, RelayCommands and EventToCommand](https://docs.microsoft.com/en-us/archive/msdn-magazine/2013/may/mvvm-commands-relaycommands-and-eventtocommand)

## Clean

- [CleanArchitectureRxSwift](https://github.com/sergdort/CleanArchitectureRxSwift)
    > Example of Clean Architecture of iOS app using RxSwift

