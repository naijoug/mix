# Tips

## Reference

- [Swift Best Practices](https://github.com/Lickability/swift-best-practices)
    > A repository that contains information related to Lickability's best practices.
- [iOS 知识小集](https://github.com/awesome-tips/iOS-Tips)
- [iOS 学习进程中遇到的知识点](https://github.com/pro648/tips)

## PATS
> PATs, Protocols with Associated Types (带有关联对象的协议)

- [Swift: 带有关联类型的协议是什么样的？](https://swift.gg/2016/09/20/swift-what-are-protocols-with-associated-types/)