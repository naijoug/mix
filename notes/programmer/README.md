# Programmer
> 👨🏻‍💻 程序员必备技能

## Reference

- [OKR](https://www.okr.com/)
    > OKR（Objectives & Key Results），中文名称是“目标与关键结果”
- [Random Street View](https://randomstreetview.com)
- [Quuick, Draw!](https://quickdraw.withgoogle.com)

## notes

- [Blogger](notes/Blogger.md)
- [GitHub](notes/GitHub.md)
- [Vim](notes/Vim.md)
- [Shell](notes/Shell.md)
- [Markdown](notes/Markdown.md)
- [Serialization](notes/Serialization.md)
- [Character Encoding](notes/CharacterEncoding.md)

- [Algorithm](algorithm/README.md)
- [Network](network/README.md)

## SCM
> Source Control Management : 源代码控制管理

- [Git](SCM/Git.md)
- [SVN](SCM/SVN.md)
- [Mercurial](SCM/Mercurial.md)

## [System](System/README.md)

- [macOS](System/macOS.md)
- [Linux](System/Linux/README.md)
- [Windows](System/Windows.md)
