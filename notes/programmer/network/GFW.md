# GFW
> Great Fire Wall 中国国家防火墙

## Reference

- [proxy](https://github.com/githubvpn007/proxy)
- [科学上网](https://github.com/haoel/haoel.github.io)
- [免费科学上网](https://github.com/Alvin9999/new-pac)
- [clash 爱好者](https://clashios.com/)
- [Paimon Node](https://pmsub.me/)
- [get_subscribe](https://github.com/ermaozi/get_subscribe)
    > ✈️ 免费机场 / 免费VPN -> 自动获取免 clash/v2ray/trojan/sr/ssr 订阅链接
- [Shadowsocks 帮助导航](https://github.com/ShadowsocksHelp/shadowsockshelp.github.io)
- [Brook, Shadowsocks, V2ray 协议层面的区别](https://talks.txthinking.com/slides/brook-ss-v2ray.slide)
- [Google Voice](https://github.com/ssnhd/googlevoice)
- [clash-rules](https://github.com/Loyalsoldier/clash-rules)
- [gfwlist](https://github.com/gfwlist/gfwlist)
- [ACL4SSR](https://github.com/ACL4SSR/ACL4SSR/tree/master)
    > SSR 去广告ACL规则/SS完整GFWList规则/Clash规则碎片
    
## Proxy
    
- [goproxy](https://github.com/snail007/goproxy)
    > Proxy 是 golang 实现的高性能http, https, websocket, tcp, socks5代理服务器,支持内网穿透,链式代理,通讯加密,智能HTTP, SOCKS5代理,黑白名单,限速,限流量,限连接数,跨平台,KCP支持,认证API。
- [shadowsocks](https://github.com/shadowsocks) : 基于 `Socks5` 代理方式的加密传输协议
    > shadowsocks is a fast tunnel proxy that helps you bypass firewalls
- [v2ray](https://github.com/v2fly/v2ray-core) : 基于 `VMess`(v2ray 自研协议)加密通讯协议
    > A platform for building proxies to bypass network restrictions
- [trojan](https://github.com/trojan-gfw/trojan)
    > An unidentifiable mechanism that helps you bypass GFW
- [goflyway](https://github.com/coyove/goflyway) : 基于 `HTTP` 协议的代理工具
    > An encrypted HTTP server
- [Brook](https://github.com/txthinking/brook)
    > A cross-platform network tool designed for developers.
- [clash](https://github.com/Dreamacro/clash)
    > A rule-based tunnel in Go.
    
## VPN

- [OpenVPN](https://github.com/OpenVPN/openvpn)
    > OpenVPN is an open source VPN daemon
- [WireGuard](https://www.wireguard.com/)
    > Fast, Modern, Secure VPN Tunnel
- [Lantern](https://github.com/getlantern/lantern)

## Client

- [Tunnelblick](https://github.com/Tunnelblick/Tunnelblick)
    > free software for OpenVPN on macOS
- [ClashX](https://github.com/yichengchen/clashX)
    > A rule based proxy For Mac base on Clash
- [V2rayU](https://github.com/yanue/V2rayU)
    > V2rayU,基于v2ray核心的mac版客户端,用于科学上网,使用swift编写,支持vmess,shadowsocks,socks5等服务协议,支持订阅, 支持二维码,剪贴板导入,手动配置,二维码分享等
- [SwitchyOmega](https://github.com/FelisCatus/SwitchyOmega)
    > Manage and switch between multiple proxies quickly & easily.
- [iGuge](https://microsoftedge.microsoft.com/addons/detail/iguge/mchibleoefileemjfghfejaggonplmmg)
    > We offer accelerated access to Google scholar Search, Chrome Store etc products.

  | `VPN` | 设备数量 | 免费流量/月 | 服务器地址 | 免费是否可用
  | :-: | :-: | :-: | :-: | :-: 
  | [Speedify](http://speedify.com/)                  | 5 | 1G    | N  | ✅ 
  | [Hotspot Shield](https://www.hotspotshield.com/)  | 1 | INF   | N  | ✅ 
  | [Windscribe](https://windscribe.com/)             | N | 10G   | 11 | ✅  
  | [SurfEasy](https://www.surfeasy.com/)             | 5 | 1.3G  | N  | ✅ 
  | [TunnelBear](https://www.tunnelbear.com/)         | N | 500M  | N  | ✅ 
  | [Hide.me](https://hide.me)                        | 1 | 2G    | 5  | ❌
  | [ProtonVPN](https://protonvpn.com)                | 1 | INF   | 3  | ❌  
  | [ZoogVPN](https://zoogvpn.com/)                   | 1 | 10G   | 3  | ❌ 
  | [ExpressVPN](https://www.expressvpn.com/)         | - | -     | -  | ❌