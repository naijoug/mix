# Windows

## Reference

- [List of files by Microsoft](https://files.rg-adguard.net/)
- [Windows ISO](https://tb.rg-adguard.net)
- [Microsoft-Activation-Scripts](https://github.com/massgravel/Microsoft-Activation-Scripts)
    > A Windows and Office activator using HWID / KMS38 / Online KMS activation methods, with a focus on open-source code and fewer antivirus detections.