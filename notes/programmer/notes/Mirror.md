# Mirror

## Reference

- [阿里云镜像](https://mirrors.aliyun.com/)
- [腾讯云镜像](https://mirrors.tencent.com/)
- [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)
- [(中科大)USTC open source software mirror.](https://mirrors.ustc.edu.cn/)