# Blogger

## Personal

- [王垠 - 狂人](http://www.yinwang.org/)
- [阮一峰](http://www.ruanyifeng.com/)
- [廖雪峰](https://www.liaoxuefeng.com/)
- [刘欣 (码农翻身)](https://shrtm.nu/fyRs)
- [Coding Tour](www.codingtour.com) : `ARTS` 实践者
- [tw93](https://tw93.fun) : Pake、MiaoYan 作者
- [王兴彬 (棒棒彬) - 字节跳动](https://github.com/Binlogo/Knowledge-Track)
- [林洵锋](https://fullstackaction.com/) : FSA全栈行动
- [郭曜源 (ibireme) - 优酷](http://blog.ibireme.com) : `YYKit` 作者、iOS RunLoop
- [孙源 (sunnyxx) - 滴滴](http://blog.sunnyxx.com) : Objective-C 深入研究
- [孙亚洲 迈腾大队长 - TME](https://www.sunyazhou.com)
- [李忠 (limboy) - 蘑菇街](http://limboy.me) : RAC
- [田伟宇 (casatwy)](https://casatwy.com) : iOS 架构系列 [](阿里 -> 得物)
- [杨萧玉 (玉令天下) - 腾讯](http://yulingtianxia.com/) : iOS Runtime、消息机制
- [张宇星 (bestswifter) - 今日头条](https://bestswifter.com/) : iOS
- [于德志 (halfrost) - 饿了么](https://halfrost.com/) : iOS 、FE、Go、机器学习
- [崔江涛 (kenshincui)](http://www.cnblogs.com/kenshincui/)
- [钟颖 (Cyan) - 微软](https://www.zhihu.com/people/ios_dev/posts) : `Pin` 作者
- [戴铭 (ming1016) - 快手](https://github.com/ming1016/study)
- [翁培钧 - 滴滴](https://github.com/windstormeye/iOS-Course)
- [图拉鼎](https://imtx.me)
- [knowledge-kit](https://github.com/FantasticLBP/knowledge-kit)
    > iOS、Web前端、后端、数据库、计算机网络、设计模式经验总结
- [zhihaozhang](https://zhihaozhang.github.io/)
- [OldBirds](https://oldbird.run/) : SwiftUI、Flutter、Vapor
- [everettjf](https://everettjf.github.io) : Fullstack
- [ExplainThis](https://www.explainthis.io/zh-hans)

## Corporate

- [🥇掘金翻译计划](https://github.com/xitu/gold-miner)
- [thoughtworks - 技术雷达](https://www.thoughtworks.com/zh-cn/radar/archive)
- [TO-D 杂志](https://github.com/zineland/2d2d)
- [天猫无线](http://pingguohe.net/)
- [美团](https://tech.meituan.com/)
- [今日头条](https://techblog.toutiao.com/)
- [微信读书](https://wereadteam.github.io/)
- [FEX - 百度前端](http://fex.baidu.com/) 
- [EFE - 百度前端](http://efe.baidu.com/)
- [FED - 淘宝前端](http://taobaofed.org/)
- [AlloyTeam - 腾讯前端](http://www.alloyteam.com/) 

## Tool

- [xLog](https://xlog.app)
- [NotionNext](https://github.com/tangly1024/NotionNext)
    > 使用 NextJS + Notion API 实现的，支持多种部署方案的静态博客，无需服务器、零门槛搭建网站，为Notion和所有创作者设计。
    > A static blog built with NextJS and Notion API, supporting multiple deployment options. No server required, zero threshold to set up a website. Designed for Notion and all creators.

- [Publish](https://github.com/johnsundell/publish)
    > A static site generator for Swift developers
    * - [用Publish创建博客](https://zhuanlan.zhihu.com/p/348232897)

- [Hexo](https://hexo.io/)
    > A fast, simple & powerful blog framework

- [Hugo](https://gohugo.io/)
    > The world’s fastest framework for building websites

- [Typecho](http://typecho.org/)
    > A PHP Blogging Platform. Simple and Powerful. 
    
- [docsifyjs](https://docsify.js.org/)
    > A magical documentation site generator.