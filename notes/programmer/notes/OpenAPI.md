# OpenAPI

## Reference

- [OpenAPI](https://github.com/OAI/OpenAPI-Specification)
    > The OpenAPI Specification Repository
- [RapidAPI Hub](https://rapidapi.com/hub)
- [free-api](https://github.com/fangzesheng/free-api)
    > 收集免费的接口服务,做一个api的搬运工
- [V2EX API](https://www.v2ex.com/help/api)
- [玩 Android 开放 API](https://www.wanandroid.com/blog/show/2)
- [Hacker News API](https://github.com/HackerNews/API)
    > Documentation and Samples for the Official HN API
- [bilibili-API-collect](https://github.com/SocialSisterYi/bilibili-API-collect)
    > 哔哩哔哩-API收集整理
- [免费节假日 API](https://timor.tech/api/holiday)
- [trace.moe-api](https://github.com/soruly/trace.moe-api)