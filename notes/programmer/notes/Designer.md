# Designer

## Reference

- [Airbnb Lottie Files](https://lottiefiles.com/)
- [及时设计](https://js.design/courses)
- [OurSketch](https://oursketch.com/)
- [Adobe全套下载](https://www.yuque.com/qianxun-nzpyh/kb)
- [设计师口袋心理学](https://iason.notion.site/6719c7650fd1406c92287f556520fbc3)

## Tool

- [Obsidian](https://obsidian.md/)
    > Obsidian 既是一个 Markdown 编辑器，也是一个知识管理软件。
- [NotionChina](https://notionchina.co/)
- [AxureChina](https://axurechina.org/)
- [axure-cn](https://github.com/pluwen/axure-cn)
- [lottie-web](https://github.com/airbnb/lottie-web)
    > Render After Effects animations natively on Web, Android and iOS, and React Native.
    > [AE如何安装bodymovin插件](https://uijiaohu.com/ae%e5%ae%89%e8%a3%85bodymovin%e6%8f%92%e4%bb%b6/)