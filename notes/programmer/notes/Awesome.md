# Awesome

## GitHub

- [GitHub Hosts](https://github.com/ineo6/hosts/) : [GitLab](https://gitlab.com/ineo6/hosts/)
    > GitHub最新hosts。解决GitHub图片无法显示，加速GitHub网页浏览。
- [gh-proxy](https://github.com/hunshcn/gh-proxy)
    > github release、archive以及项目文件的加速项目
- [starlist.dev](https://starlist.dev/)
    > 🤩 Top 1000 GitHub Repositories Ranked by Stars
- [Github1s](https://github.com/conwnet/github1s) : 通过在线 VSCode 查看 Github 仓库代码
    > One second to read GitHub code with VS Code.
- [Awesome-GitHub-Repo](https://github.com/Wechat-ggGitHub/Awesome-GitHub-Repo)
    > 收集整理 GitHub 上高质量、有趣的开源项目。
- [GitHub 漫游指南](https://github.com/phodal/github)
    > a Chinese ebook on how to build a good project on Github. Explore the users' behavior. Find some thing interest.
- [Hacker Laws](https://github.com/dwmkerr/hacker-laws)
    > Laws, Theories, Principles and Patterns that developers will find useful.

## Awesome

- [awesome](https://github.com/sindresorhus/awesome)
    > 😎 Awesome lists about all kinds of interesting topics
- [awesome-cn](https://github.com/icopy-site/awesome-cn)
    > 超赞列表合集
- [free-programming-books](https://github.com/EbookFoundation/free-programming-books)
    > 📚 Freely available programming books
- [CS-Notes](https://github.com/CyC2018/CS-Notes)
    > 📚 技术面试必备基础知识、Leetcode、计算机操作系统、计算机网络、系统设计
- [Tutorial](https://github.com/zhonghuasheng/Tutorial)
    > 后端 （Java Golang）全栈知识架构体系总结
- [CodeGuide | 程序员编码指南](https://github.com/fuzhengwei/CodeGuide)
- [小林 x 图解计算机基础](https://github.com/xiaolincoder/CS-Base)
    > 图解计算机网络、操作系统、计算机组成、数据库，共 1000 张图 + 50 万字，破除晦涩难懂的计算机基础知识，让天下没有难懂的八股文！

## 📚

- [shu](https://github.com/shjwudp/shu)
    > 中文书籍收录整理
- [chinese-poetry](https://github.com/chinese-poetry/chinese-poetry)
    > The most comprehensive database of Chinese poetry 🧶最全中华古诗词数据库, 唐宋两朝近一万四千古诗人, 接近5.5万首唐诗加26万宋诗. 两宋时期1564位词人，21050首词。
- [RoadMap](https://github.com/xx-zh/xx-zh-roadmap)
- [书栈网](https://www.bookstack.cn/)
- [BookStash](https://bookstash.io)
    > Top books recommended by famous folk, in 3m or less.
- [给程序员的开源、免费图书集合](https://github.com/waylau/books-collection)
- [免费的编程中文书籍索引](https://github.com/justjavac/free-programming-books-zh_CN)
- [Free Programming Books](https://github.com/EbookFoundation/free-programming-books)
- [Be a professional programmer](https://github.com/stanzhai/be-a-professional-programmer)
- [DevOps Guidebook](https://github.com/tsejx/devops-guidebook)
    > 📚 DevOps 知识图谱 关于Linux、服务器、数据库、部署等相关体系

## Tutorials

- [Coding Interview University](https://github.com/jwasham/coding-interview-university)
    > A complete computer science study plan to become a software engineer.
- [Tutorials for Web Developers](https://github.com/StevenSLXie/Tutorials-for-Web-Developers)
    > Tutorials for web developers including bash scripting, Linux commands, MongoDB. Actively updating.
- [sast-skill-docs](https://github.com/SAST-skill-docers/sast-skill-docs) : [SAST Skill Docs](https://docs.net9.org)
    > SAST Skill Docs. We are paving the way for your CS studying.
- [💻cs-408](https://github.com/ddy-ddy/cs-408)
    > 计算机考研专业课程408相关的复习经验，资源和OneNote笔记
- [浙江大学课程攻略共享计划](https://github.com/QSCTech/zju-icicles)
- [SICP 计算机程序的构造和解释](https://github.com/DeathKing/Learning-SICP)
- [Open Source Society University](https://github.com/ossu/computer-science)
    > 🎓 Path to a free self-taught education in Computer Science!
- [The Missing Semester of Your CS Education](https://github.com/missing-semester-cn/missing-semester-cn.github.io)
    > the CS missing semester Chinese version
- [IoT For Beginners](https://github.com/microsoft/IoT-For-Beginners)
    > 2 Weeks, 24 Lessons, IoT for All!
- [ML For Beginners](https://github.com/microsoft/ML-For-Beginners)
    > 12 weeks, 26 lessons, 52 quizzes, classic Machine Learning for all

## Interviews

- [反向面试](https://github.com/yifeikong/reverse-interview-zh)
- [技术面试必备基础知识](https://github.com/CyC2018/CS-Notes)
- [android-interview](https://github.com/guoxiaoxing/android-interview)
- [IT 行业应试学知识库](https://github.com/apachecn/Interview)

## Tool

- [RSSHub](https://github.com/DIYgod/RSSHub)
    > 🍰 Everything is RSSible
- [Fig](https://github.com/withfig/autocomplete) : 终端自动补全
    > Fig adds autocomplete to your terminal.
- [ipatool](https://github.com/majd/ipatool) : 命令行下载 AppStore 软件
    > Command-line tool that allows searching and downloading app packages (known as ipa files) from the iOS App Store
- [wakapi](https://github.com/muety/wakapi)
    > 📊 A minimalist, self-hosted WakaTime-compatible backend for coding statistics
- [NeoVim](https://github.com/neovim/neovim)
    > Vim-fork focused on extensibility and usability
- [TabNine](https://github.com/codota/TabNine) : AI 写代码
    > AI Code Completions
- [Copilot](https://github.com/features/copilot) : GitHub AI 程序员
    > Your AI pair programmer
- [HTTPie](https://github.com/httpie/httpie)
    > 🥧 HTTPie for Terminal — modern, user-friendly command-line HTTP client for the API era. JSON support, colors, sessions, downloads, plugins & more.
- [Talk](https://github.com/vasanthv/talk) : [Talk](https://tlk.li)
    > Group video call for the web.

## Animation

- [cubic-bezier](http://cubic-bezier.com)
- [Easing Functions Cheat Sheet](https://github.com/ai/easings.net)

## Browser

- [Tampermonkey](https://www.tampermonkey.net/)
    > 一款免费的浏览器扩展和最为流行的用户脚本管理器
- [Greasy Fork](https://greasyfork.org/zh-CN)
- [OpenUserJS](https://openuserjs.org/)
- [Google Chinese ResultsBlocklist](https://github.com/cobaltdisco/Google-Chinese-Results-Blocklist)

## Money

- [How to make more money](https://github.com/easychen/howto-make-more-money)
    > 程序员如何优雅的挣零花钱？
- [DevMoneySharing](https://github.com/loonggg/DevMoneySharing)
    > 独立开发者赚钱经验分享
    
## Funny

- [ASCII generator](https://github.com/uvipen/ASCII-generator)
    > ASCII generator (image to text, image to image, video to video)
- [文言 wenyan-lang](https://github.com/wenyan-lang/wenyan)
    > 文言文編程語言 A programming language for the ancient Chinese.
- [Seeing Theory](https://github.com/seeingtheory/Seeing-Theory)
    > A visual introduction to probability and statistics.
- [State-of-the-Art Shitcode Principles](https://github.com/trekhleb/state-of-the-art-shitcode)
    > 💩State-of-the-art shitcode principles your project should follow to call it a proper shitcode
- [This-repo-has-N-stars](https://github.com/fslongjin/This-repo-has-916-stars)
    > 当star的数量发生改变时，项目名称会被动态地更新。
- [sao-gen-gen](https://github.com/disksing/sao-gen-gen)
    > 骚话生成器生成器
- [Thief](https://github.com/cteamx/Thief)
    > 一款基于 `Electron` 开发的创新跨平台摸鱼神器
- [程序员考公指南](https://github.com/coder2gwy/coder2gwy)