# English

## Reference

- [English-level-up-tips-for-Chinese](https://github.com/byoungd/English-level-up-tips-for-Chinese)
    > An advanced guide to learn English which might benefit you a lot 🎉 . 可能是让你受益匪浅的英语进阶指南。
- [Everyone can user English](https://github.com/xiaolai/everyone-can-use-english)
    > 人人都能用英语
- [A programmer guide to English](https://github.com/yujiangshui/A-Programmers-Guide-to-English)
    > 专为程序员编写的英语学习指南。
- [Chinese programmer wrong pronunciation](https://github.com/shimohq/chinese-programmer-wrong-pronunciation)
    > 中国程序员容易发音错误的单词
- [english-note](https://github.com/hzpt-inet-club/english-note)
    > 从0开始学习英语语法
- [Qwerty Learner](https://github.com/Kaiyiwing/qwerty-learner)
    > 为键盘工作者设计的单词记忆与英语肌肉记忆锻炼软件