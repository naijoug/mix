# notes

## [programmer](./programmer/README.md)

- [Git](./programmer/SCM/Git.md)
- [Shell](./programmer/topics/Shell.md)
- [Linux](./programmer/system/Linux/README.md)

## [programming](./programming/README.md)

- [Swift](./programming/Swift/README.md)
- [JavaScript](./programming/JavaScript/README.md)
- [Python](./programming/Python/README.md)
- [Java](./programming/Java/README.md)
- [C++](./programming/C++/README.md)
- [Ruby](./programming/Ruby/README.md)

## [apple](./apple/README.md)

- [iOS](./apple/iOS/README.md)
- [macOS](./apple/macOS/README.md)

## [frontend](./frontend/README.md)

- [HTML](./frontend/web/HTML/README.md)
- [React](./frontend/web/React.md)
- [Vue](./frontend/web/Vue.md)
- [Android](./frontend/mobile/Android/README.md)
- [Flutter](./frontend/mobile/Flutter.md)
- [ReactNative](./frontend/mobile/ReactNative.md)
- [Weex](./frontend/mobile/Weex.md)

## [backend](./backend/README.md)

- [server](./backend/server/README.md)
- [service](./backend/service/README.md)
- [database](./backend/database/README.md)
