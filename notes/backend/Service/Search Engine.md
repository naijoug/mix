# Search Engine

## Reference

- [lucene](https://github.com/apache/lucene)
    > Apache Lucene is a high-performance, full-featured text search engine library written in Java.
- [Elasticsearch](https://github.com/elastic/elasticsearch)
    > Free and Open, Distributed, RESTful Search Engine
- [meilisearch](https://github.com/meilisearch/meilisearch)
    > ⚡ A lightning-fast search engine that fits effortlessly into your apps, websites, and workflow 🔍
- [Sphinx](https://github.com/sphinxsearch/sphinx)
    > Sphinx search server
- [RediSearch](https://github.com/RediSearch/RediSearch)
    > A query and indexing engine for Redis, providing secondary indexing, full-text search, and aggregations.
- [Xunsearch](https://github.com/hightman/xunsearch)
    > （中文名：迅搜）免费开源的中文搜索引擎，采用 C/C++ 编写 (基于 xapian 和 scws)，提供 PHP 的开发接口和丰富文档
- [TNTSearch](https://github.com/teamtnt/tntsearch)
    > A fully featured full text search engine written in PHP