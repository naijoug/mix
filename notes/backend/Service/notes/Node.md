# Node

## Reference

- [Node](https://github.com/nodejs/node)
- [Node 入门](https://www.nodebeginner.org/index-zh-cn.html)
- [七天学会NodeJS](https://nqdeng.github.io/7-days-nodejs/)
- [极简 Node.js 入门教程](https://www.yuque.com/sunluyong/node)

## koa

- [koa](https://github.com/koajs/koa)
    > Expressive middleware for node.js using ES2017 async functions
- [koa-docs-Zh-CN](https://github.com/demopark/koa-docs-Zh-CN.git)
- [koa examples](https://github.com/koajs/examples)
- [koa-typescript-cms](https://github.com/murongg/koa-typescript-cms)
- [Node 安全指南](https://github.com/Tencent/secguide/blob/main/JavaScript%E5%AE%89%E5%85%A8%E6%8C%87%E5%8D%97.md#2)
- [Koa 错误处理](https://github.com/demopark/koa-docs-Zh-CN/blob/master/error-handling.md)

## sequelize

- [Sequelize](https://github.com/sequelize/sequelize) : [docs](https://sequelize.org/)
    > Feature-rich ORM for modern Node.js and TypeScript, it supports PostgreSQL (with JSON and JSONB support), MySQL, MariaDB, SQLite, MS SQL Server, Snowflake, Oracle DB (v6), DB2 and DB2 for IBM i.
- [sequelize-docs-Zh-CN](https://github.com/demopark/sequelize-docs-Zh-CN) : [Sequelize 中文文档](https://www.sequelize.cn/)
- [sequelize `!:` 语法](https://sequelize.org/v5/manual/typescript.html)
- [sequelize-typescript](https://github.com/sequelize/sequelize-typescript)
    > Decorators and some other features for sequelize