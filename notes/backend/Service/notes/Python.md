# Python

## Flask

- [Flask](https://github.com/pallets/flask)
    > The Python micro framework for building web applications.
- [Flask之旅](https://github.com/spacewander/explore-flask-zh)
- [Flask 中文文档](https://github.com/dormouse/Flask_Docs_ZhCn)

## FastAPI

- [FastAPI](https://github.com/tiangolo/fastapi)
    > FastAPI framework, high performance, easy to learn, fast to code, ready for production