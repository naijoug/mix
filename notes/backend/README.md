# Backend

## Reference

- [互联网公司经典技术架构](https://github.com/davideuler/architecture.of.internet-product)
- [深入架构原理与实践](https://github.com/isno/theByteBook)
    > 深入讲解内核网络、Kubernetes、ServiceMesh、容器等云原生相关技术，大厂人都在看的书籍。

## Concept

| abbr | full | description
| -- | -- | --
| -     | Microservice                      | 微服务，独立开发和部署服务的开发体系架构
| SOA   | Service Oriented Architecture     | 面向服务的架构，基于网络通用的通信语言的服务接口，让软件组件可重复利用。
| SOAP  | Simple Object Access Protocol     | 简单对象协议 
| WSDL  | Web Services Description Language | 基于 XML 的 Web 服务描述语言
| ESB   | Enterprise Service Bus            | 企业服务总线
| RPC   | Remote Procedure Call             | 远程过程调用
| REST  | Representational State Transfer   | 表现层状态转化  
| SSO   | Single Sign On                    | 单点登录
| JWT   | JSON Web Token                    | 
| OAuth | Open Authorization                | 
| CMS   | Content Management System         | 内容管理系统
| CRUD  | Create Read Update Delete         | 增删改查
| CORS  | Cross-Origin Resource Sharing     | 跨域资源共享
| NIO   | NoneBlocking IO                   |
| BIO   | Blocking IO                       |
| VPC   | Virtual Private Cloud             | 


