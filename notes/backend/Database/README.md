# Database
> 数据库

## notes

- [SQL](notes/SQL.md)
- [SQLite](notes/SQLite.md)
- [MySQL](notes/MySQL.md)
- [MongoDB](notes/MongoDB.md)

## Reference

- [db-tutorial](https://github.com/dunwu/db-tutorial)
    > 📚 db-tutorial 是一个数据库教程。
- [设计数据密集型应用](https://github.com/Vonng/ddia)
    > 《Designing Data-Intensive Application》DDIA中文翻译
    > [一本神书：《数据密集型应用系统设计》](https://fuxuemingzhu.cn/ddia)
    
## ClickHouse
    
- [ClickHouse](https://github.com/ClickHouse)
    > ClickHouse® is a free analytics DBMS for big data
- [ClickHouse Docs](https://clickhouse.com/docs/zh/)