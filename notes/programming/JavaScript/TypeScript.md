# TypeScript

## Reference

- [TypeScript](https://github.com/microsoft/TypeScript)
    > TypeScript is a superset of JavaScript that compiles to clean JavaScript output.
- [typescript-tutorial](https://github.com/xcatliu/typescript-tutorial) : [TypeScript 入门教程](https://ts.xcatliu.com/)
- [TypeScript Docs](https://www.typescriptlang.org/docs) : [TypeScript 中文网](https://ts.nodejs.cn/docs/)
- [TypeScript](https://github.com/zhongsp/TypeScript) : [TypeScript 使用指南手册](https://www.patrickzhong.com/TypeScript)
- [typescript-book](https://github.com/basarat/typescript-book)
    > 📚 The definitive guide to TypeScript and possibly the best TypeScript book 📖. Free and Open Source 🌹
- [typescript-book-chinese](https://github.com/jkchao/typescript-book-chinese) : [TypeScript Deep Dive 中文版](https://jkchao.github.io/typescript-book-chinese)
- [clean-code-typescript](https://github.com/labs42io/clean-code-typescript) : [TypeScript 代码整洁之道](https://github.com/pipiliang/clean-code-typescript)

## Tips

- [2022.03.18 慎用 TypeScript 感叹号『非空断言』操作符](https://juejin.cn/post/7076293418988601375)
- [2021.07.06 一篇够用的TypeScript总结](https://juejin.cn/post/6981728323051192357)
- [2020.09.14 一份不可多得的 TS 学习指南](https://juejin.cn/post/6872111128135073806)