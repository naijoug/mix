# Java

## Reference

- [OpenJDK](https://github.com/openjdk)
- [💯 Java 全栈知识体系](https://pdai.tech)
- [toBeBetterJavaer](https://github.com/itwanger/toBeBetterJavaer)
    > 一份通俗易懂、风趣幽默的Java学习指南，内容涵盖Java基础、Java并发编程、Java虚拟机、Java企业级开发、Java面试等核心知识点。学Java，就认准Java程序员进阶之路😄
- [JCSprout](https://github.com/crossoverJie/JCSprout)
    > 👨‍🎓 Java Core Sprout : basic, concurrent, algorithm
- [HOW2J](https://how2j.cn/)
- [Java 教程](https://www.liaoxuefeng.com/wiki/1252599548343744)
- [📚 OnJava8](https://github.com/LingCoder/OnJava8)
    > On Java 8》中文版
- [📚 think-in-java](https://github.com/quanke/think-in-java)
    > Thinking in Java (Java 编程思想)

## Note

- [Java](notes/Java.md)
- [Java X](notes/JavaX.md)
- [Java API](notes/JavaAPI.md)

## OracleJDK vs OpenJDK

## Java Web

- Web Application
    * Servlet/JSP
    * JSTL/JSF
    * Struts/Spring MVC
- Web Service
    * JAX-WS
    * JAX-RS

## JAX-WS vs JAX-RS

* JAX-WS (Java API for XML Web Services) : XML(SOAP/WSDL)
    > 动词为中心，指定每次执行的函数
- JAX-RS (Java API for RESTful Web Services) : JSON
    > 名称为中心，指定每次执行的资源
    * [Jersey](https://github.com/eclipse-ee4j/jersey) : JAX-RS 的一个实现

## Jetty vs Tomcat
> 相同点: 都是 Servlet 引擎

## Libraries

- [Gson](https://github.com/google/gson) 
    > A Java serialization/deserialization library to convert Java Objects into JSON and back
- [Guava](https://github.com/google/guava)
    > Google core libraries for Java
- [JUnit](https://github.com/junit-team/junit5)
    > The programmer-friendly testing framework for Java and the JVM

## Build

- [Ant]
- [Maven](notes/Maven.md)
- [Gradle](notes/Gradle.md)

## Netty

- [通俗地讲，Netty 能做什么？](https://www.zhihu.com/question/24322387)

## [Activiti](notes/Activiti.md)

## Kafka

- [Kafka简明教程](https://zhuanlan.zhihu.com/p/37405836)

## Tool

- [Symphony](https://github.com/88250/symphony)
    > 🎶 一款用 Java 实现的现代化社区（论坛/问答/BBS/社交网络/博客）系统平台。
    >
    > Jul 7, 2022A modern community (forum/Q&A/BBS/SNS/blog) system platform implemented in Java.