# Vue

## Reference

- [Vue](https://github.com/vuejs/core)
    > 🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
- [Nuxt](https://github.com/nuxt/nuxt)
    > Nuxt is an intuitive and extendable way to create type-safe, performant and production-grade full-stack web apps and websites with Vue 3.

## UI

- [Element](https://github.com/ElemeFE/element)
    > A Vue.js 2.0 UI Toolkit for Web
- [Element-Plus](https://github.com/element-plus/element-plus)
    > 🎉 A Vue.js 3 UI Library made by Element team
- [ViewUI](https://github.com/view-design/ViewUI)
    > A high quality UI Toolkit built on Vue.js 2.0
- [ViewUIPlus](https://github.com/view-design/ViewUIPlus)
    > An enterprise-level UI component library and front-end solution based on Vue.js 3
    
## Tutorial

- [2023-05-29 Vue 和 React 的区别看这里](https://juejin.cn/post/7238199999733088313)