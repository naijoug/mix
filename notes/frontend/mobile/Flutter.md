# Flutter
> Flutter makes it easy and fast to build beautiful apps for mobile and beyond

## Reference

- [Flutter](https://github.com/flutter/flutter)
- [Flutter 中文网](https://flutter.cn)
- [Flutter 实战](https://github.com/flutterchina/flutter_in_action_2nd)
- [Flutter开发实战详解系列](https://github.com/CarGuo/gsy_flutter_book)
    > 本系列将完整讲述：如何快速从 0 开发一个完整的 Flutter APP，配套高完成度 Flutter 开源项目 GSYGithubAppFlutter ，同时会提供一些Flutter的开发细节技巧，之后深入源码和实战为你全面解析 Flutter 。

