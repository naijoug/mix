# Android

## Reference

- [Kotlin](https://github.com/JetBrains/kotlin)
- [Kotlin 语言中文站](https://github.com/hltj/kotlin-web-site-cn)

## Collector

- [2023-06-20 Netflix Android 客户端开源库大公开](https://juejin.cn/post/7246453307735392316)

## Concept

- Android 系统体系结构

    | 体系结构体 | 说明
    | --- | ---
    | `Applications`                | 应用程序，系统自带的一些应用程序(Email、SMS...)
    | `Application Frameworks`      | 应用程序框架， 提供应用程序开发的 API
    | `Libraries & Android Rumtime` | 系统运行库，和 Android 运行时环境
    | `Linux Kernel`                | Linux 内核