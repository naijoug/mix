# Frontend


## Reference

- [MDN Web Technology](https://developer.mozilla.org/zh-CN/docs/Web)
- [Front-End-Develop-Guide](https://github.com/icepy/Front-End-Develop-Guide)
    > 💰 Awesome The Front End Develop Guide
- [Front End Handbook](https://github.com/dwqs/front-end-handbook)
- [CN-VScode-Docs](https://github.com/jeasonstudio/CN-VScode-Docs)
    > VScode 说明文档翻译
- [Web](https://github.com/qianguyihao/Web)
    > 千古前端图文教程，超详细的前端入门到进阶知识库。从零开始学前端，做一名精致优雅的前端工程师。
- [dev-zuo 笔记](https://fe.zuo11.com/) : 
- [30 seconds of code](https://github.com/30-seconds/30-seconds-of-code)
    > Short JavaScript code snippets for all your development needs

## `npm` vs `yarn` vs `pnpm`

- [npm](https://github.com/npm/cli)
    > the package manager for JavaScript
- [yarn](https://github.com/yarnpkg/yarn)
    > Fast, reliable, and secure dependency management.
- [pnpm](https://github.com/pnpm/pnpm) : [中文文档](https://pnpm.io/zh/)
    > Fast, disk space efficient package manager

## Usage

```shell
# npm
$ npm get registry  # 查看到当前镜像
$ npm config set registry https://registry.npmmirror.com # 设置为阿里镜像
$ npm config set registry https://registry.npmjs.org/
# yarn
$ yarn config get registry 
$ yarn config set registry https://registry.npmmirror.com
$ yarn config set registry https://registry.yarnpkg.com
```

## Concept

- SPA (Single Page Applaction) : 单页面应用

## Frameworks

- [nest](https://github.com/nestjs/nest) : [中文文档](https://docs.nestjs.cn/)
    > A progressive Node.js framework for building efficient, scalable, and enterprise-grade server-side applications on top of TypeScript & JavaScript (ES6, ES7, ES8) 🚀
- [next](https://github.com/vercel/next.js) : [中文文档](https://www.nextjs.cn/)
    > The React Framework
- [egg](https://github.com/eggjs/egg) : [中文文档](https://www.eggjs.org/zh-CN)
    > 🥚 Born to build better enterprise frameworks and apps with Node.js & Koa
- [Strapi](https://github.com/strapi/strapi)
    > 🚀 Strapi is the leading open-source headless CMS. It’s 100% JavaScript, fully customizable and developer-first.
- [Driver.js](https://github.com/kamranahmedse/driver.js)
    > A light-weight, no-dependency, vanilla JavaScript engine to drive the user's focus across the page

## Web Cross Platform

- [uni-app](https://github.com/dcloudio/uni-app)
    > A cross-platform framework using Vue.js
- [taro](https://github.com/NervJS/taro)
    > 开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信/京东/百度/支付宝/字节跳动/ QQ 小程序/H5/React Native 等应用。
- [kbone](https://github.com/Tencent/kbone)
    > 一个致力于微信小程序和 Web 端同构的解决方案
- [wepy](https://github.com/Tencent/wepy)
    > 小程序组件化开发框架
- [chameleon](https://github.com/didi/chameleon)
    > 🦎 一套代码运行多端，一端所见即多端所见
- [mpx](https://github.com/didi/mpx)
    > Mpx，一款具有优秀开发体验和深度性能优化的增强型跨端小程序框架
- [mpvue](https://github.com/Meituan-Dianping/mpvue)
    > 基于 Vue.js 的小程序开发框架，从底层支持 Vue.js 语法和构建工具体系。

## Native Corss Platform

- [NativeScript](https://github.com/NativeScript/NativeScript)
    > ⚡ Empowering JavaScript with native platform APIs. ✨ Best of all worlds (TypeScript, Swift, Objective C, Kotlin, Java). Use what you love ❤️ Angular, Capacitor, Ionic, React, Svelte, Vue and you name it compatible.
- [SCADE](https://www.scade.io)
    > Native App Development with Swift for iOS and Android
- [RubyMotion](http://www.rubymotion.com)
    > 使用 Ruby 开发 iOS, Android 以及 OS X 原生应用
- [Flybirds](https://github.com/ctripcorp/flybirds)
    > 基于自然语言的，跨端跨框架 BDD UI 自动化测试方案，BDD testing, Python style, Present by Trip Flight

## Tool

- [Web Check](https://github.com/lissy93/web-check): [Web Check](https://web-check.xyz)
    > 🕵️‍♂️ All-in-one OSINT tool for analysing any website
- [Seeing-Theory](https://github.com/seeingtheory/Seeing-Theory)
    > A visual introduction to probability and statistics.
- [WebdriverIO](https://github.com/webdriverio/webdriverio)
    > Next-gen browser and mobile automation test framework for Node.js
- [Jest](https://github.com/jestjs/jest)
    > Delightful JavaScript Testing.
- [npmgraph](https://github.com/npmgraph/npmgraph)
    > A tool for exploring NPM modules and dependencies

## UI

- [Primer](https://primer.style/)
    > Primer Design, build, and create with GitHub’s design system
- [Ant Desgin](https://ant.design)
    > An enterprise-class UI design language and React UI library

## Monorepo

- [Lerna](https://github.com/lerna/lerna)
    > 🐉 Lerna is a fast, modern build system for managing and publishing multiple JavaScript/TypeScript packages from the same repository.
- [nx](https://github.com/nrwl/nx)
    > Smart, Fast and Extensible Build System