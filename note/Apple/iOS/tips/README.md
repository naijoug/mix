# Tips

## Reference

- [Swift Best Practices](https://github.com/Lickability/swift-best-practices)
    > A repository that contains information related to Lickability's best practices.
- [iOS 知识小集](https://github.com/awesome-tips/iOS-Tips)
- [iOS 学习进程中遇到的知识点](https://github.com/pro648/tips)
